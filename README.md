# Prowl

## What is Prowl?
Prowl is an email harvesting tool that scrapes Yahoo for Linkedin profiles associated to the users search terms and identifies job titles. It also identifies current job listings for the specififed organisation. 

I updated this tool to Python3, as I still find it has some value, but not enough to go through the hassle of installing python2. 

### Documentation
[Here](https://github.com/nettitude/prowl/wiki/Documentation)

### Backstory
[![Steelcon 2017 - Neil Lines - Samurai of the west](https://img.youtube.com/vi/3kHP5D7VZ_I/hqdefault.jpg)](https://youtu.be/3kHP5D7VZ_I?t=6m47s)<br />
(CLICK TO WATCH)

