#!/usr/bin/env python3
"""
This script is designed to scrape LinkedIn for company staff members, and optionally find available jobs and subdomains.

It uses command-line arguments to specify the company to search for, the email address format to use, and the depth of the search. The script can also check for updates on GitHub and prompt the user to update if a new version is available.
"""
import os
import git
import argparse
import requests

parser = argparse.ArgumentParser(description="Scrape LinkedIn for company staff members")
parser.add_argument("-c", "--company", help="Company to search for")
parser.add_argument("-e", "--emailformat", help='Format of house email address style. Use: <fn>,<ln>,<fi>,<li> as placeholders for first/last name/initial. e.g "<fi><ln>@company.com"')
parser.add_argument("-j", "--jobs",  help="Include available jobs in result", action='store_true')
parser.add_argument("-s", "--subdomains",  help="Find subdomains", action='store_true')
parser.add_argument("-a", "--all",  help="FIND EVERYTHING", action='store_true')
parser.add_argument("-d", "--depth", help="How many pages of Yahoo to search through", default='10')
parser.add_argument("-p", "--proxy", help="http(s)://localhost:8080", default='')
args = parser.parse_args()

def welcome():
        print("_"*50)
        print(''' \n    
  _____     ______    ______  ___  ___  ___ ___
 /      \  /      \  /      \ |  \ |  \ |  \| $$
|  $$$$$$\|  $$$$$$\|  $$$$$$\| $$ | $$ | $$| $$
| $$  | $$| $$   \$$| $$  | $$| $$ | $$ | $$| $$
| $$__/ $$| $$      | $$__/ $$| $$_/ $$_/ $$| $$
| $$    $$| $$       \$$    $$ \$$   $$   $$| $$
| $$$$$$$  \$$        \$$$$$$   \$$$$$\$$$$  \$$
| $$
| $$
 \$$    \n         ''')
        print("Author: @MattSPickford, Updated by: @StochasticSanity")
        print("_"*50)
        print("")
welcome()

response = requests.get('https://raw.githubusercontent.com/StochasticSanity/Prowl/master/Version.txt')
html = response.text
openfile = open('Version.txt', 'r')
cversion = openfile.readline()

if (html != cversion):
        Join = input('New version found, would you like to update? (Y/N)').lower()
        if Join == 'y':
                print("Pr0wl is not up to date, updating...")
                os.system("git fetch --all")
                os.system("git reset --hard origin/master")
                g = git.cmd.Git("https://github.com/pickfordmatt/Prowl")
        if Join == 'n':
                pass
        print("")
        print("")

import file
