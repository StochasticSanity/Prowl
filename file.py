#!/usr/bin/env python3
"""
This script is designed to scrape LinkedIn for company staff members, and optionally find available jobs and subdomains.

It uses command-line arguments to specify the company to search for, the email address format to use, and the depth of the search. The script can also check for updates on GitHub and prompt the user to update if a new version is available.
"""
import argparse
import dns.resolver
import os
import re
import requests
import string
import sys
import time

from bs4 import BeautifulSoup

parser = argparse.ArgumentParser(description="Scrape LinkedIn for company staff members")
parser.add_argument("-c", "--company", help="Company to search for")
parser.add_argument("-e", "--emailformat", help='Format of house email address style. Use: <fn>,<ln>,<fi>,<li> as placeholders for first/last name/initial. e.g "<fi><ln>@company.com"')
parser.add_argument("-j", "--jobs", help="Exclude available jobs in result", action='store_true')
parser.add_argument("-s", "--subdomains",  help="Find subdomains", action='store_true')
parser.add_argument("-a", "--all",  help="FIND EVERYTHING", action='store_true')
parser.add_argument("-d", "--depth", help="How many pages of Yahoo to search through", default='10')
parser.add_argument("-p", "--proxy", help="http(s)://localhost:8080", default='')
args = parser.parse_args()

found = []
emails=[]
proxies = ''
names = []
blocks = []

TIMEOUT = 10

class bcolors:
    OKGREEN = '\033[92m'
    WARNING = '\033[31m'
    FAIL = '\033[33m'

def set_proxy():
    """
    Set the proxy from the command line arguments, if provided.
    """
    proxy = args.proxy
    if proxy:
        if 'https' in proxy:
            proxies_format = {'https': proxy}
        else:
            proxies_format = {'http': proxy}
    return proxies_format

def certscanner(emailformat):
    """
    Scan for subdomains using the crt.sh website.

    :param emailformat: Email format string to derive domain and company name
    """
    dns_resolver = dns.resolver.Resolver()
    dns_resolver.nameservers = ['8.8.8.8']
    domain_name = emailformat.split("@")[1]
    company = domain_name.split('.', 1)[0]
    company_name = domain_name.split(".")[0]
    target = open("Output/"+company_name+"/domains.csv", 'w+', encoding="utf-8")
    target.write("Domain, IP"+"\n")
    print(bcolors.OKGREEN + "\nSUBDOMAINS")
    print("-"*40)
    html = requests.get("https://crt.sh/?q=%."+domain_name+"&exclude=expired", timeout=TIMEOUT).text
    soup = BeautifulSoup(html, "html.parser")
    for link in soup.findAll("a"):
        if "?id=" in link.get('href'): 
            link_id = link.get('href')
            cert = requests.get("https://crt.sh/"+link_id, timeout=TIMEOUT).text
            myregex = r'(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}'
            domains = re.findall(myregex, cert)
            for i in domains:
                if company in i:
                    if i in found:
                        pass
                    else:
                        found.append(i)
                        try:
                            public_ip = dns_resolver.query(i)[0]
                            print(bcolors.FAIL + i, public_ip)
                            target.write(i + "," + str(public_ip) + "\n")
                        except dns.exception.DNSException:
                            public_ip = ""
                            print(bcolors.WARNING + i)
                            target.write(i + "\n")

def formatout(company_name, emailformat):
    """
    Create the output folder structure and initial files.

    :param companyname: The name of the company
    :param emailformat: The email format string
    """
    domain_name = emailformat.split("@")[1]
    company_name = domain_name.split(".")[0]
    if not os.path.exists("Output/" + company_name):
        os.makedirs("Output/" + company_name + "/")
        with open("Output/" + company_name + "/accounts.csv", 'w', encoding="utf-8") as target:
            target.write("First Name, Last Name, Email, Title, Profile, Breach" + "\n")
        with open("Output/" + company_name + "/domains.csv", 'w', encoding="utf-8") as target:
            pass
    print("_" * 50 + "\n")


def bing(comp, emailformat):
    """
    Search for LinkedIn profiles on Bing.

    :param comp: Company name
    :param emailformat: Email format string
    """
    print("\n" + bcolors.OKGREEN + "BING")
    print("-"*125)
    for i in range(0,40,1):
        url = 'http://www.bing.com/search?q=site:linkedin.com+works+at+' + comp + '&first=' + str(i)
        res = requests.get(url, timeout=TIMEOUT)
        soup = BeautifulSoup(res.text, "lxml")
        div = ' '
        for tag in soup.find_all("li" , {"class" : "b_algo"}):
            if re.search("linkedin.com/in/", str(tag)):
                if re.search("Top", tag.getText()):
                    pass
                else:
                    if tag.getText() not in blocks:
                        title = tag.find("h2")
                        cut = title.getText().split(div,2)[:2]
                        name = cut[0]+" "+cut[1]
                        time.sleep(0.1)
                        blocks.append(tag.getText())
                        if name not in names:
                            names.append(name)
                            linkedinurl = tag.find("cite").getText()
                            try:
                                fulljob = tag.find("li").text
                                mangle_emails(name, emailformat, fulljob, linkedinurl)
                            except:
                                fulljob = ""
                                mangle_emails(name, emailformat, fulljob, linkedinurl)


def search(comp, emailformat):
    """
    Search for LinkedIn profiles on Yahoo.

    :param comp: Company name
    :param emailformat: Email format string
    """
    depth = int(args.depth)
    print("\n")
    print(bcolors.OKGREEN + "YAHOO")
    print("-" * 125)
    for i in range(1, depth):
        try:
            r = requests.get('https://uk.search.yahoo.com/search;_ylt=A9mSs3IiEdVYsUMAY6ZLBQx.;_ylu=X3oDMTEzdm1nNDAwBGNvbG8DaXIyBHBvcwMxBHZ0aWQDBHNlYwNwYWdpbmF0aW9u?p="at+{0}"+site%3Alinkedin.com&pz=10&ei=UTF-8&fr=yfp-t-UK317&b={1}0&pz=10&xargs=0'.format(comp, i), proxies=proxies, timeout=TIMEOUT)
        except:
            print("Proxy Not Working")
            print("Exiting....")
            sys.exit(1)

        soup = BeautifulSoup(r.text, "lxml")
        for tag in soup.find_all("div", {"class": "dd algo algo-sr Sr"}):
            if re.search("linkedin.com/in/", str(tag)):
                if re.search("Top", tag.getText()):
                    pass
                else:
                    if tag.getText() not in found:
                        title = tag.find("h3", {"class": "title"})
                        div = " "
                        cut = title.getText().split(div, 2)[:2]
                        name = cut[0] + " " + cut[1]
                        if name not in names:
                            jobtext = tag.find("p", {"class": "lh-16"})
                            job = jobtext.getText()
                            href = tag.find("a")
                            linkpof = ""
                            linkpof = href['href']
                            found.append(tag.getText())
                            if "View " in job:
                                fulljob = " "
                                mangle_emails(name, emailformat, fulljob, linkpof)


def mangle_emails(name, emailformat, fulljob, linkpof):
    """
    Generate emails based on the email format and check for breaches.

    :param name: Full name of the person
    :param emailformat: Email format string
    :param fulljob: Job title or position
    :param linkpof: LinkedIn profile URL
    """
    name = name.replace(",", "")
    domain_name = emailformat.split("@")[1]
    company_name = domain_name.split(".")[0]
    target = open("Output/"+company_name+"/accounts.csv", 'a', encoding="utf-8")
    fn = name.split()[0]
    fi = fn[0]
    ln = name.split()[1]
    li = ln[0]
    email = emailformat.replace('<fn>',fn).replace('<ln>',ln).replace('<fi>',fi).replace('<li>',li).lower()
    email2 = ''.join(filter(lambda x: x in string.printable, email))
    headers = {
        'User-Agent': 'Prowl'
    }

    if email2 not in emails:
        try:
            time.sleep(1.5)
            emails.append(email2)
            fulljob = fulljob.replace(",", " ")
            req = requests.get("https://haveibeenpwned.com/api/breachedaccount/"+email2+"?truncateResponse=true", headers=headers, timeout=TIMEOUT)
            breach = str(req.content)
            breach = breach.replace(",", " ")
            try:
                target.write(fn+","+ln+","+email2+","+fulljob+","+linkpof+","+breach+"\n")
                print("{0:30} {1:40} {2:40}".format(name, email2, fulljob[0:40]))
            except Exception as e:
                print(f"Error writing to file: {e}")
                try:
                    print("{0:30} {1:40}".format(name, email2))
                    target.write(fn+","+ln+","+email2+",N/A,"+linkpof+","+breach+"\n")
                except Exception as e:
                    print(f"Error writing to file: {e}")
        except Exception as e:
            print(f"Error in main try block: {e}")
            emails.append(email2)
            try:
                target.write(fn+"|"+ln+"|N/A|"+linkpof+"\n")
            except Exception as e:
                print(f"Error writing to file: {e}")
    else:
        pass

def jobs(comp, emailformat):
    """
    Search for available jobs at the company using Indeed.

    :param comp: Company name
    :param emailformat: Email format string
    """
    domain_name = emailformat.split("@")[1]
    company_name = domain_name.split(".")[0]
    target = open("Output/"+company_name+"/jobs.csv", 'w', encoding="utf-8")
    target.write("Job Title"+"\n")
    print("\n")
    print(bcolors.OKGREEN + "AVAILABLE JOBS")
    print("-"*40)
    r = requests.get("https://www.indeed.co.uk/jobs?as_and='at+{0}'&as_phr=&as_any=&as_not=&as_ttl=&as_cmp=&jt=all&st=&salary=&radius=25&l=&fromage=any&limit=30&sort=&psf=advsrch".format(comp), timeout=TIMEOUT)
    soup = BeautifulSoup(r.text, "lxml")
    for tag in soup.findAll("h2", {"class" : "jobtitle"}):
        job = str(tag.getText().strip())
        print(bcolors.FAIL + job)
        target.write(job + "\n")

if args.proxy:
    proxies = set_proxy()

if args.subdomains is True:
    if args.emailformat:
        domain = args.emailformat.split("@")[1]
        companyname = domain.split(".")[0]
        print("Output folder: "+companyname)
        formatout(args.company, args.emailformat)
        certscanner(args.emailformat)
else:
    pass

if args.company:
    if args.emailformat:
        formatout(args.company, args.emailformat)

if args.jobs is True:
    jobs(args.company, args.emailformat)
else:
    pass

if args.all is True:
    certscanner(args.emailformat)
    jobs(args.company, args.emailformat)
else:
    pass

if args.company:
    if args.emailformat:
        bing(args.company, args.emailformat)
        search(args.company, args.emailformat)
else:
    parser.print_usage()